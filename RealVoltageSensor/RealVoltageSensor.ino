/*
Cheap eBay “Voltage Sensor Module”
Measures 0.02445V to 25V (Supposedly. My guess is 0.1V to 24.9V)
Original text from seller (Cleaned up a little):

This module is based on the principle of resistive voltage dividers,
it can make the terminal connector input voltage 5 times smaller.
Arduino analog input voltages up to 5V, the voltage detection module
input voltage not greater than 5Vx5=25V (if using 3.3V systems, input
voltage not greater than 3.3Vx5=16.5V). Arduino AVR chips have 10-bit
AD, so this module simulates a resolution of 0.00489V (5V/1023), so
the minimum voltage of input voltage detection module is 0.00489Vx5=0.02445V.

This example code is in the public domain.

Wiring: Arduino 5V to Sensor ‘+’
Arduino GND to Sensor ‘-’
Arduino A0 to Sensor ‘S’

*/

float volt; //Create a variable to store the measurement.
void setup()
{
Serial.begin(9600); //Start Serial Communications
}
void loop()
{
volt=(analogRead(0)/4.092)/10; //Read Analog Pin0, divide that by 4.092, and divide whats left by 10
/*Note: The value 4.092 was part of the (non-compiling) code included with the ‘voltage sensor’
that I purchase from eBay. I’m not 100% sure how it was calculated, but it works with a resolution
down to 0.1 so I kept using it. (It shows value to .01, but rounds up on the last digit) */

Serial.print(volt); //Print the the variable to the serial terminal.
Serial.println(“V”); //Makes it pretty, also prints newline character.
delay(1000); //Delays for 1 second. Without this, it would be hard to read the values.
}
