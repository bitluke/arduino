#include <WiFlyHQ.h>
#include <SoftwareSerial.h>
SoftwareSerial wifiSerial(8, 9);
#include <OneWire.h>
#include <TempSensor.h>

// Transmit LED
#define LED 2
WiFly wifly;
/* Change these to match your WiFi network */
const char mySSID[] = "ut-public";
const char myPassword[] = "";
// buffer used to write to serial terminal
char buf[80];
// server address
const char server[] = "54.82.220.45";

void setup() {
    Serial.begin(9600);
    Serial.println(F("Initializing Software."));
    Serial.print(F("Free memory: "));
    Serial.println(wifly.getFreeMemory(), DEC);
    // connect and initialize the wifly
    startWifiConnection();


    if (wifly.open(server, 80)) {
        Serial.print(F("Connected to "));
        Serial.println(server);
        sendWifiStats();
        //wifly.close();
    } else {
        Serial.println(F("Failed to connect"));
    }
    Serial.println(F("Software initialized."));
}

void loop() {
    delay(13000);
    if (wifly.open(server, 80)) {
        Serial.print(F("Connected to "));
        Serial.println(server);
        sendWifiStats();
    } else {
        Serial.println(F("Failed to connect"));
    }
}

void startWifiConnection() {
    Serial.println(F("Attempting connection to wifly"));
    wifiSerial.begin(9600);
    if (!wifly.begin(&wifiSerial, &Serial)) {
        Serial.println(F("Failed connection to wifly"));
    }
    /* Join wifi network if not already associated */
    if (!wifly.isAssociated()) {
        /* Setup the WiFly to connect to a wifi network */
        Serial.println(F("Joining network"));
        wifly.setSSID(mySSID);
        wifly.setPassphrase(myPassword);
        wifly.enableDHCP();
        wifly.save();
        if (wifly.join()) {
            Serial.println(F("Joined wifi network"));
        } else {
            Serial.println(F("Failed to join wifi network"));
            wifly.terminal();
        }
    } else {
        Serial.println(F("Already joined network"));
    }
    wifly.setBroadcastInterval(0); // Turn off UPD broadcast
    //prints info about the wifi
    wifiDiagnostic();

    wifly.setDeviceID("Wifly-WebServer");
    if (wifly.isConnected()) {
        Serial.println(F("Old connection active. Closing"));
        wifly.close();
    }
    wifly.setProtocol(WIFLY_PROTOCOL_TCP);
    if (wifly.getPort() != 80) {
        wifly.setPort(80);
        /* local port does not take effect until the WiFly has rebooted (2.32) */
        wifly.save();
        Serial.println(F("Set port to 80, rebooting to make it work"));
        wifly.reboot();
        delay(3000);
    }
    Serial.println(F("WiFi Ready"));
}

void wifiDiagnostic() {
    Serial.print(F("MAC: "));
    Serial.println(wifly.getMAC(buf, sizeof (buf)));
    Serial.print(F("IP: "));
    Serial.println(wifly.getIP(buf, sizeof (buf)));
    Serial.print(F("Netmask: "));
    Serial.println(wifly.getNetmask(buf, sizeof (buf)));
    Serial.print(F("Gateway: "));
    Serial.println(wifly.getGateway(buf, sizeof (buf)));
    Serial.print(F("RSSI: "));
    Serial.print(wifly.getRSSI());
    Serial.println(F(" dBm"));
    Serial.print(F("Uptime (seconds): "));
    Serial.println(wifly.getUptime());
    Serial.print(F("Rate: "));
    Serial.println(wifly.getRate());
}

void sendWifiStats() {
    int rssi = wifly.getRSSI();
    int uptime = wifly.getUptime();
    int freeMemory = wifly.getFreeMemory();
    Serial.print(F("GET /updatestats.php?rssi="));
    Serial.print(rssi);
    Serial.print(F("&uptime="));
    Serial.print(uptime);
    Serial.print(F("&free="));
    Serial.print(freeMemory, DEC);
    Serial.println(F(" HTTP/1.0"));
    Serial.println();
    Serial.println(F("Stats sent to server"));
}
