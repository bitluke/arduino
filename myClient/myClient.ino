#include <TinkerKit.h>

// creating the object 'pot' that belongs to the 'TKPotentiometer' class
TKPotentiometer pot(I0);
 
// creating the object 'led' that belongs to the 'TKLed' class 
TKLed led(O0);                              
 
int brightnessVal = 0;  // value read from the pot
 
void setup() {
  // initialize serial communications at 9600 bps
  Serial.begin(9600);
}
 
void loop() {
  // read the potentiometer's value:
  brightnessVal = pot.read();            
  map(brightnessVal, 0, 1023, 0, 5);
  // set the led brightness
  led.brightness(brightnessVal);       
 
  // print the results to the serial monitor:
  Serial.print("brightness = " );                      
  Serial.println(brightnessVal);      
 
  // wait 10 milliseconds before the next loop
  delay(10);                    
}
