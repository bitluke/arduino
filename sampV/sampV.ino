void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);  
}

void loop() {
  int val = analogRead(A0);
  float volts = (val / 1023.0) * 5;
  Serial.println(volts);
}
