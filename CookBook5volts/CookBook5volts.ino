const int batteryPin = 0;

void setup()
{
  Serial.begin(9600);
}

void loop(){
  //readvoltage();
  //Serial.println("***********");
  //loopvolts();
  Serial.println("Voltage");
  //Serial.println(readvoltage());
}

void loopvolts()
{
  int val = analogRead(batteryPin); // read the value from the sensor
 
  long mv =  (val * (500000/1023L)) / 100; // calculate the value in millivolts
  Serial.print("mv");
  Serial.println(mv/1000);
  Serial.print(mv/1000); // print the integer value of the voltage  
  Serial.print('.');
  int fraction = (mv % 1000); // calculate the fraction
  Serial.print("frac");
  Serial.println(fraction);
  if (fraction == 0)
     Serial.print("000");      // add three zero's
  else if (fraction < 10)    // if fractional < 10 the 0 is ignored giving a wrong 
                              // time, so add the zeros
     Serial.print("00");       // add two zeros
  else if (fraction < 100)
     Serial.print("0"); 
  Serial.println(fraction); // print the fraction
  
  
}


float readvoltage(){
  int val = analogRead(batteryPin); // read the value from the sensor
 
  long mv =  (val * (500000/1023L)) / 100; // calculate the value in millivolts

  float decimalPart;
  float fraction = (mv % 1000); // calculate the fraction
  //Serial.print("frac");
  //Serial.println(fraction);
  if (fraction == 0){
     fraction *= 0.0001;      // add three zero's
  }else if (fraction < 10){    // if fractional < 10 the 0 is ignored giving a wrong          
  // time, so add the zeros
     fraction *= 0.001;       // add two zeros
  }else if (fraction < 100){
      fraction *= 0.01; 
  }else{ 
      fraction *= 0.1;
  }    
  
//  Serial.println("frac");
//  Serial.println(fraction);
  return (float) (mv/1000) + fraction;
  
}
