#include <avr/io.h>
#include <avr/interrupt.h>

//existing library
#include <Ports.h>
#include <WiFlyHQ.h>
#include <SensorProtocol.h>
#include <TinkerKit.h>
#include <aJSON.h>
#include <MemoryFree.h>
#define NUM_SAMPLES 10
#define LEDPIN 13

ISR(WDT_vect) { Sleepy::watchdogEvent(); }

//server parameters
char* server = "54.82.220.45";
int serverPort = 8080;
uint32_t idleCounts;

//voltage settings
int sum = 0;                    // sum of samples taken
unsigned char sample_count = 0; // current sample number
float voltage = 0.0;            // calculated voltage
int avgWakeUpTime = 5;
WiFly wifly;

/* Change these to match your WiFi network (client parameters) */
const char mySSID[] = "ut-public";
const char myPassword[] = "";

int sensorTypes[] = {1, // temperature. 
2, //hall sensor
3, //light sensor
4, //battery status
5 //cpu usage.
};

int locationID = 1;
int length = sizeof(sensorTypes) / sizeof(int);
SensorProtocol protocol(locationID);

//declaring TKSensors and actuators
TKLed red(O0);
TKLed yellow(O4);
TKLed green(O2);
TKLed blue(O3);
TKThermistor thermistor(I0);
TKHallSensor hall(I1);
TKLightSensor light(I2);



uint32_t Idle_Counter;
uint8_t CPU_Utilization_Info_Read_To_Compute;
uint32_t Prev_Idle_Counter;
uint32_t Idle_Counts;
uint32_t Calculate_Idle_Counts (void);

void initTimer2(void){
  //8 bit timer. we want a 1ms compare match interrupt
  //16Mhz main clock. 16Mhz / 1024 = 15625hz = 64us
  //target interval = tick time * (timer counts + 1)
  //1ms = 64us * (x + 1)
  //(1ms / 64us) - 1 = x = 14.625
  //
  //16Mhz/256 = 62500hz = 0.000016s
  //(1ms / 16us) - 1 = x = 61.5
  //
  //16Mhz/128 = 125000hz = 0.000008s
  //(1ms/8us) - 1 = x = 124
  // set compare match register to desired timer count:
  // fclkio / (2 * prescaler (OCR2A)) = f
  // 16000000 / (2 * 128 (1 + 124)) =  500hz

  OCR2A = 124;  

  // turn on CTC(clear timer on compare match) mode:
  // TCCR2A = 0;
  TCCR2A |= (1 << WGM21);

  // Set CS20 and CS22 bits for 128 prescaler:
  // TCCR2B = 0; 
  // TCCR2B |= (1 << CS20);
  TCCR2B |= (1 << CS22);

  // enable timer compare match interrupt:
  TIMSK2 |= (1 << OCIE2A);
}

//For existing setUP
void initSetup(){
  analogReference(DEFAULT);
  red.on();
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial1.println("Serial1 started");
  protocol.setSensors(sensorTypes, length);
  getConnected();
  getConnectedWithServer();
}


void setup(){
  pinMode(LEDPIN, OUTPUT);

  // initialize the serial communication:
  initSetup();

  noInterrupts();

  initTimer2();

  // enable global interrupts:
  interrupts();
  Serial.println("started");

}

void Signal_Idle(void){
  Idle_Counter++; 
}

//connections
void getConnected() {
  while(!wifly.begin(&Serial, &Serial1)) {
    Serial1.println("Failed to start wifly");
    delay(1000);
  }
  
  wifly.setJoin(1);
  wifly.setWakeTimer(0);
  wifly.setFlushSize(1420);
  wifly.setTxPower(10);
  

  /* Join wifi network if not already associated */
  if (!wifly.isAssociated()) {
    /* Setup the WiFly to connect to a wifi network */
    Serial1.println("Joining network");
    wifly.setSSID(mySSID);
    wifly.setPassphrase(myPassword);
    wifly.enableDHCP();
    
    while(!wifly.join()) {
      Serial1.println("Join failed");
    } 
    Serial1.println("Joined wifi network");
  } 
  else {
    Serial1.println("Already joined network");
  }
  Serial1.println("WiFly ready");

  red.off();
  green.off();
  yellow.on();

  wifly.setDeviceID("Wifly-TCP");
  wifly.setIpProtocol(WIFLY_PROTOCOL_TCP);

  if (wifly.isConnected()) {
    Serial1.println("Old connection active. Closing");
    wifly.close();
  }
  wifly.save();
}

void getConnectedWithServer() {
  while(!wifly.open(server,serverPort, true)){
    Serial1.println("TCP Connection failed");
    wifly.close(); // Weirdly connection remains activte sometimes but isConnected returns false
    delay(1000);
  }
  green.on();
  yellow.off();
  red.off();
  Serial1.println("connected");
}

void postData() {
  float tm = thermistor.getCelsius();
  float hs = hall.get();
  float ldr = light.get();
  
  protocol.addValue(1, tm);
  protocol.addValue(2, hs);
  protocol.addValue(3, ldr);
  protocol.addValue(4, readVoltage());
  protocol.addValue(5, Calculate_CPU_Utilization(idleCounts));
  
  Message msg = protocol.createMessage();
  wifly.println("POST /data/ HTTP/1.1");
  wifly.print("Host: ");
  wifly.println(server);
  wifly.println("Connection: close");
  wifly.println("Content-Type: application/json");
  wifly.print("Content-Length: ");
  wifly.println(msg.length);
  wifly.println();
  wifly.println(msg.message);
}

int readResponse() {
  Serial1.println("readResponse");
  long startMillis = millis();
  boolean message = false;
  String line = "";
  long contentLength = 0;
  long read = 0;
  while (!(message && read >= contentLength) && (millis() - startMillis) < 5000) {
    while (wifly.available() > 0) {
      char ch = wifly.read();      
      line += ch;
      startMillis = millis(); 
      if (message) {
        read += 1;  
      } else if (line.endsWith("\r\n")) {
        if (line == "\r\n") {
          message = true;
        } else if (line.length() > 16 && line.startsWith("Content-Length: ")) {
          contentLength = line.substring(16, line.length() - 2).toInt();
        }
        line = "";
      }
    }
  }
  
  int idleTime = 0;
  if (line != "") {
    Serial1.print("Json: ");
    Serial1.println(line);
    char buf[line.length() + 1];
    line.toCharArray(buf, sizeof(buf));
    aJsonObject *root = aJson.parse(buf);
    aJsonObject *idle = aJson.getObjectItem(root, "idle");
    if (idle->type == aJson_Int) {
      idleTime = idle->valueint;
    }
    aJson.deleteItem(root);
  } else {
    Serial1.println("Invalid message received");
  }
  return idleTime; 
}

void flicker(TKLed *led) {
  led->on();
  delay(50);
  led->off(); 
}

float readVoltage(){
   // take a number of analog samples and add them up
    while (sample_count < NUM_SAMPLES) {
        sum += analogRead(A2);
        sample_count++;
        delay(10);
    }
    // calculate the voltage
    // use 5.0 for a 5.0V ADC reference voltage
    // 5.015V is the calibrated reference voltage
    voltage = ((float)sum / (float)NUM_SAMPLES * 5) / 1024.0;
    // send voltage for display on Serial Monitor
    // voltage multiplied by 11 when using voltage divider that
    // divides by 11. 11.132 is the calibrated voltage divide
    // value
    wifly.print(voltage);
    wifly.println (" V");
    sample_count = 0;
    sum = 0;
    return voltage;
}


uint32_t Read_Idle_Counts(void){
  uint32_t rv;
  noInterrupts();//reading 4bytes is not atomic on an 8bit arch
  rv = Idle_Counts;
  interrupts();
  return rv;
}

#define UNLOADED_IDLE_COUNTS 174848

uint32_t Calculate_CPU_Utilization (uint32_t temp_counts){
  return 100 - ((100 * temp_counts) / UNLOADED_IDLE_COUNTS);
}

uint32_t Calculate_Idle_Counts (void){
  uint32_t temp_counts;
  Idle_Counts = Idle_Counter - Prev_Idle_Counter;
  Prev_Idle_Counter = Idle_Counter;
  return Idle_Counts;
}


uint8_t One_MS_Task_Ready;
uint8_t Ten_MS_Task_Ready;
uint8_t Twenty_MS_Task_Ready;
uint8_t One_Hundred_MS_Task_Ready;
uint8_t One_S_Task_Ready;

void One_MS_Task(void){
}
void Ten_MS_Task(void){
}
void One_Hundred_MS_Task(void){
}



void One_S_Task(void){
  idleCounts = Calculate_Idle_Counts();
   firstloop();
   
  //Serial.print("CPU Utilization  ");
  //Serial.print(Calculate_CPU_Utilization(idleCounts));
  //Serial.println("%");
}


void Run_Tasks(void){
  
  if(One_MS_Task_Ready)
  {
    
    One_MS_Task_Ready=0;
    One_MS_Task();
  }
  if(Ten_MS_Task_Ready)
  {
   
    Ten_MS_Task_Ready=0;
    Ten_MS_Task();
  }
  if(One_Hundred_MS_Task_Ready)
  {
    
    One_Hundred_MS_Task_Ready=0;
    One_Hundred_MS_Task();
  }
  if(One_S_Task_Ready)
  {
    
    One_S_Task_Ready=0;
    One_S_Task();
  }
}
// #define INTERVAL 100
// uint16_t previousMillis;
void loop(){
  Signal_Idle();
  Run_Tasks();
}

/* WARNING this function called from ISR */
void Update_Task_Ready_Flags(void){
  static uint16_t counter;
  One_MS_Task_Ready=1;
  counter++;
  if((counter%10)==0)
  {
    Ten_MS_Task_Ready=1;
  }
  if((counter%100)==0)
  {
    One_Hundred_MS_Task_Ready=1;;
  }
  if(counter == 1000)
  {
    One_S_Task_Ready=1;
    counter=0;
  }  
}

void firstloop(){
    //Serial1.print("Loop start at ");
  //Serial1.println(millis());
  
  if(!wifly.isConnected()) {
    green.off();
    yellow.off();
    red.on();
    
    // reconnect
    getConnectedWithServer();
  }
  //Serial1.print("Connected at ");
  //Serial1.println(millis());
  
  blue.on();
  postData();
  int idle = readResponse();
  blue.off();
  
  Serial1.print("Data sent at ");
  Serial1.println(millis());
  
  Serial1.print(">>> Available memory= ");
  Serial1.println(freeMemory());
  
  if (idle > 65) { // Max is 65 seconds for now..
    idle = 65;
  }
  
  idle = idle - avgWakeUpTime;  
  if (idle > 0) {
    wifly.close();
    delay(100);
    wifly.sleep();
    delay(100);
    Sleepy::loseSomeTime(idle * 1000);
  } 
}

ISR(TIMER2_COMPA_vect){
//1ms isr
  Update_Task_Ready_Flags();//keep this isr short and sweet
}    
