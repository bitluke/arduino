#include <TinkerKit.h>

#define NUM_SAMPLES 10
#define BATT_VOLTAGE 8.78

// creating the object 'pot' that belongs to the 'TKPotentiometer' class
TKPotentiometer pot(I0);

int sum = 0;                    // sum of samples taken
unsigned char sample_count = 0; // current sample number
float voltage = 0.0;            // calculated voltage
int brightnessVal = 0;  // value read from the pot

void setup()
{
    Serial.begin(9600);
    
}

void loop()
{ 
    Serial.print("Potentio readstep"); 
    Serial.println(pot.readStep(10));
    Serial.print("Potentio read"); 
    Serial.println(pot.read());
  
    // take a number of analog samples and add them up
    while (sample_count < NUM_SAMPLES) {
        sum += analogRead(A11);
        sample_count++;
        delay(10);
    }
    // calculate the voltage
    // use 5.0 for a 5.0V ADC reference voltage
    // 5.015V is the calibrated reference voltage
    voltage = ((float)sum / (float)NUM_SAMPLES * BATT_VOLTAGE) / 1024.0;
    // send voltage for display on Serial Monitor
    // voltage multiplied by 11 when using voltage divider that
    // divides by 11. 11.132 is the calibrated voltage divide
    // value
    Serial.print(voltage);
    Serial.println (" V");
    sample_count = 0;
    sum = 0;
}
